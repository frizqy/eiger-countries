<?php

namespace App\Http\Controllers;

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class WelcomeController extends Controller
{
    public function index(Request $request)
    {
        $response = Http::get('https://restcountries.com/v3.1/all', []);
        $responseArray = $response->object();
        usort($responseArray, function ($a, $b) {
            return $a->name->common <=> $b->name->common;
        });
        $datas = $this->paginate($responseArray);

        return view('welcome', ['datas' => $datas]);
    }

    public function paginate($items, $perPage = 1, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
